# Dangerous Sidekiq Change Detector

Determines whether a change violates [these principles](https://docs.gitlab.com/ee/development/sidekiq/compatibility_across_updates.html#changing-the-arguments-for-a-worker) by introducing a change to a Sidekiq worker API in such a way that may not be safely deployable.

As this is a proof of concept, I've used two project folders. In practice, the different code sources would probably be pulled out using something like:

```bash
# Get the list of filenames that the current MR touches.
git diff --name-only main
# Get the original file content
git show main:path/to/file.rb
# Get the file content from the current HEAD
git show HEAD:path/to/file.rb
```


## Example

The commit `a7ccffb` introduces a worker and its user in `sample_apps/git/` and commit `c3c2575` makes a risky update. Running `./check_with_git.rb` currently gives us the following output:

```
Detected a change to sample_apps/git/jobs/my_worker.rb
  - Checking whether arity of `perform` method changed?
  - Was 1, now 2
  - Checking other changed files for updated usages
  - ⚠️  Potentially unsafe update made to sample_apps/git/jobs/my_worker.rb in sample_apps/git/lib/trigger.rb
```

See `check_with_git.rb` for how this case might be handled.
