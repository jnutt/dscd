#!/usr/bin/env ruby

require_relative 'lib/worker_parser'
require_relative 'lib/usage_finder'

original_arity = WorkerParser.new(File.read('sample_apps/a/jobs/my_worker.rb')).perform_method_arity
new_arity = WorkerParser.new(File.read('sample_apps/b/jobs/my_worker.rb')).perform_method_arity

exit if new_arity == original_arity

puts "uh oh, the arity has changed! (#{original_arity} vs #{new_arity}) we'd better be careful!"

other_changed_files = Dir['sample_apps/b/lib/**']

other_changed_files.each do |file|
  usage_finder = UsageFinder.new(File.read(file), :MyWorker)

  if usage_finder.arities.any? { |a| a == new_arity }
    puts "warning! the worker and its usage have updated arity in the same MR!"
  end
end
