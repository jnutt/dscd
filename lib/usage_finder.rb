require 'parser/current'

Usage = Struct.new(:arity)

class UsageFinder < Parser::AST::Processor
  attr_reader :usages

  def initialize(raw_code, klass)
    @usages = []
    @klass = klass

    ast = Parser::CurrentRuby.parse(raw_code)
    process(ast)
  end

  def on_send(node)
    receiver_node, method_name, *arg_nodes = *node

    if receiver_node.children[1] == @klass && method_name == :perform_async
      @usages << Usage.new(arg_nodes.length)
    end

    super(node)
  end

  def arities
    usages.collect(&:arity)
  end
end
