require 'parser/current'

class WorkerParser < Parser::AST::Processor
  attr_reader :worker_class, :perform_method_arity

  def initialize(raw_code)
    @data = Parser::CurrentRuby.parse(raw_code)
    process(@data)
  end

  def on_class(node)
    @worker_class = node.children[0].children[1]
    super(node)
  end

  def on_def(node)
    name, args_node, _body_node = *node

    @perform_method_arity = args_node.children.length if name == :perform

    super(node)
  end
end
