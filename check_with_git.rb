#!/usr/bin/env ruby

require_relative 'lib/worker_parser'
require_relative 'lib/usage_finder'

updated_files = `git diff --name-only a7ccffb..c3c2575`.lines.map(&:chomp)

updated_jobs, other_files = updated_files.partition { |file_name| file_name.include?('/jobs/') }

updated_jobs.each do |updated_job|
  puts "Detected a change to #{updated_job}"

  puts "  - Checking whether arity of `perform` method changed?"
  original_arity = WorkerParser.new(`git show a7ccffb:#{updated_job}`).perform_method_arity

  new_parser = WorkerParser.new(`git show c3c2575:#{updated_job}`)
  new_arity = new_parser.perform_method_arity

  next if original_arity == new_arity
  puts "  - Was #{original_arity}, now #{new_arity}"
  puts "  - Checking other changed files for updated usages"

  other_files.each do |other_file|
    usage_finder = UsageFinder.new(`git show c3c2575:#{other_file}`, new_parser.worker_class)

    if usage_finder.arities.any? { |a| a == new_arity }
      puts "  - ⚠️  Potentially unsafe update made to #{updated_job} in #{other_file}"
    end
  end
end
