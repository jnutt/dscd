require_relative '../../lib/usage_finder'

RSpec.describe UsageFinder do
  let(:single_usage_example) {
    <<~RUBY
    def single_usage
    MyWorker.perform_async(123)
    end
    RUBY
  }

  let(:double_usage_example) {
    <<~RUBY
    def multiple_usages
      MyWorker.perform_async(123, "blah", foo: :bar)
      MyWorker.perform_async(123,
        "blah",
        "extra",
        foo: :bar)
    end
    RUBY
  }

  context 'with one argument' do
    let(:raw_code) {
      <<~RUBY
      MyWorker.perform_async(123)
      RUBY
    }

    it 'detects the arity' do
      arity = described_class.new(raw_code, :MyWorker).arities.first
      expect(arity).to eq(1)
    end
  end

  context 'with two arguments' do
    let(:raw_code) {
      <<~RUBY
      MyWorker.perform_async(123, "blah")
      RUBY
    }

    it 'detects the arity' do
      arity = described_class.new(raw_code, :MyWorker).arities.first
      expect(arity).to eq(2)
    end
  end

  context 'with more complex arguments' do
    let(:raw_code) {
      <<~RUBY
      MyWorker.perform_async(123, "blah", foo: :bar)
      RUBY
    }

    it 'detects the arity' do
      arity = described_class.new(raw_code, :MyWorker).arities.first
      expect(arity).to eq(3)
    end
  end

  context '#usages' do
    it 'counts single usages' do
      usages = described_class.new(single_usage_example, :MyWorker).usages
      expect(usages.length).to eq(1)
    end

    it 'counts multiple usages' do
      usages = described_class.new(double_usage_example, :MyWorker).usages
      expect(usages.length).to eq(2)
    end
  end
end