require_relative '../../lib/worker_parser'

RSpec.describe WorkerParser do
  context 'with one argument' do
    let(:raw_code) {
      <<~RUBY
      class MyWorker
        def perform(one_arg); end
      end
      RUBY
    }

    it 'detects the arity' do
      arity = described_class.new(raw_code).perform_method_arity
      expect(arity).to eq(1)
    end
  end

  context 'with two arguments' do
    let(:raw_code) {
      <<~RUBY
      class MyWorker
        def perform(one_arg, two_args); end
      end
      RUBY
    }

    it 'detects the arity' do
      arity = described_class.new(raw_code).perform_method_arity
      expect(arity).to eq(2)
    end
  end

  context 'with more complex arguments' do
    let(:raw_code) {
      <<~RUBY
      class MyWorker
        def perform(one_arg, another = 1, foo: 2); end
      end
      RUBY
    }

    it 'detects the arity' do
      arity = described_class.new(raw_code).perform_method_arity
      expect(arity).to eq(3)
    end
  end

  context '.worker_class' do
    let(:raw_code) { File.read('sample_apps/b/jobs/my_worker.rb') }

    it 'can tell us the worker class name' do
      klass = described_class.new(raw_code).worker_class
      expect(klass).to eq(:MyWorker)
    end
  end

  context 'worker is in module' do
    let(:raw_code) { File.read('sample_apps/b/jobs/job_in_module.rb') }

    it 'picks up the class name' do
      klass = described_class.new(raw_code).worker_class
      expect(klass).to eq(:SomeWorker)
    end
  end
end