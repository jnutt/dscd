class Trigger
  def do_something
    MyWorker.perform_async("hello world", "unsafe_addition")
  end
end